from tkinter import *
import subprocess
import socket
import pickle

root = Tk()
root.title('Mac-Scavenger')
root.geometry("400x400")

colour = StringVar()
colour.set('red')

def create_message(name, hostname, port):
    return {"name": name, "hostname": hostname, "port": port }

def send_data_to_server(name, port):
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.settimeout(120)

    #TODO replace it with IP adress of production server
    host = '192.168.1.103'

    try:
        print("Tries to establish connection")
        client_socket.connect((host, port))
        print("Connected to server")
        client_socket.settimeout(None)

        client_hostname = socket.gethostname()

        dict = create_message(name, client_hostname, port)
        msg = pickle.dumps(dict)
        condition = True

        while condition:
            client_socket.send(msg)

            response = client_socket.recv(1024).decode("utf-8")
            print('response was ' + response)

            if response == "OK":
                condition = False

        client_socket.close()
        return True
    except:
        return False


def startMonitor():
    warning_label = Label(root, fg=colour.get(), text="You need to give it a name")

    name_monitor = textBox.get()
    port = 2000

    if not textBox.get():
        warning_label.place(relx = 0.5, rely = 0.2, anchor = CENTER)
        return

    #For some reason it is not working.
    warning_label.place_forget()

    print("Downloading necessary libraries")
    returned_value = subprocess.call(['sh', './config.sh'])
    print(returned_value)

    print("Sending link info through socket to server")
    if not send_data_to_server(name_monitor, port):
        return

    print("Starting Monitor ("+name_monitor+") on port ("+str(port)+") ...")
    returned_value = subprocess.call(['python3', '../MacScavengerMonitor.py', name_monitor, str(port)])

    print("Monitor running")

def endMonitor():
    print("Stopping Monitor ...")
    root.destroy()


label = Label(root, text="Enter a device name")
textBox = Entry(root, width=30)

button = Button(root, text="Start Monitor", command=startMonitor)
button_exit = Button(root, text="Stop Monitor", command=endMonitor)

label.place(relx = 0.5, rely = 0.3, anchor = CENTER)
button.place(relx = 0.5, rely = 0.5, anchor = CENTER)
button_exit.place(relx = 0.5, rely = 0.6, anchor = CENTER)
textBox.place(relx = 0.5, rely = 0.4, anchor = CENTER)

root.mainloop()
