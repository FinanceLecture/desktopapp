#!/bin/bash
echo "Welcome to MacScavenger"
echo "Install dependencies"

when_failure () {
	echo "Installation of package $1 failed"
	echo "Check if pip3 is installed with: pip3 --version"
	echo "Check if python3 is installed with: python --version"
}

echo "Install all for Yaml"
sudo pip3 install wheel || when_failure "wheel"
sudo pip3 install setuptools || when_failure "setuptools"
sudo pip3 install pyyaml || when_failure "pyyaml"

echo "Install all dependencies for Pandas"
sudo pip3 install python-dateutil || when_failure "dateutil"
sudo pip3 install pyth || when_failure "pytz"
sudo pip3 install cython || when_failure "cython"
sudo pip3 install numpy || when_failure "numpy"
sudo pip3 install pandas || when_failure "pandas"


echo "Install all for Matlab"
sudo pip3 install streamz || when_failure "streamz"
sudo pip3 install pymongo || when_failure "pymongo"
sudo pip3 install sympy || when_failure "sympy"
sudo pip3 install nose || when_failure "nose"
sudo apt-get install python3-matplotlib || when_failure "matplotlib"

echo "Install rest for MacScavengerShell"
sudo apt-get install python3-scipy || when_failure "scipy" 
sudo apt-get install python3-shapely || when_failure "shapely"
sudo pip3 install termtables || when_failure "termtables"

echo "Install rest for MacScavengerMonitor"
sudo apt-get install python3-psutil || when_failure "psutil"
sudo pip3 install pyric || when_failure "pyric"

