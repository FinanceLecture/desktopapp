import socket
import pickle

def create_message(name, hostname, port):
    return {"name": name, "hostname": hostname, "port": port }

def send_data_to_server(name, port):
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.settimeout(120)

    #TODO replace it with IP adress of production server
    host = '192.168.1.103'

    try:
        print("Tries to establish connection")
        client_socket.connect((host, port))
        print("Connected to server")
        client_socket.settimeout(None)

        client_hostname = socket.gethostname()

        dict = create_message(name, client_hostname, port)
        msg = pickle.dumps(dict)
        condition = True

        while condition:
            client_socket.send(msg)

            response = client_socket.recv(1024).decode("utf-8")
            print('response was ' + response)

            if response == "OK":
                condition = False

        client_socket.close()
        return True
    except:
        return False
